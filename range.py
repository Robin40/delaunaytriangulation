from constants import eps


class Range(object):
    def __init__(self, start, stop):
        self.start = start
        self.stop = stop

    def __len__(self):
        return self.stop - self.start

    def __contains__(self, value):
        return self.start - eps <= value <= self.stop + eps

    @staticmethod
    def bounding(values):
        return Range(min(values), max(values))

    def with_margin(self, d):
        return Range(self.start - d, self.stop + d)
