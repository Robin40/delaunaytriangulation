from numpy.linalg import det

from collisions import bounding_box, eps
from point import Point
from triangle import Triangle


def big_triangle_containing(points):
    bbox = bounding_box(points)
    rect = bbox.with_margin(8)
    xr, yr = rect.x_range, rect.y_range
    a = Point(xr.start, yr.start)
    b = Point(xr.start + len(xr)*2, yr.start)
    c = Point(xr.start, yr.start + len(yr)*2)
    return Triangle(a, b, c)


def big_triangles_containing(points):
    bbox = bounding_box(points)
    rect = bbox.with_margin(32)
    a, b, c, d = rect.points()
    return Triangle(a, b, c), Triangle(c, d, a)


def circle_test(*points):
    return det([[p.x, p.y, p.sq_norm(), 1] for p in points])


def legalized(g, side):
    neigh = side.inv
    if neigh not in g:
        return g

    quad_sides = g.ccw[side], g.cw[side], g.ccw[neigh], g.cw[neigh]
    a, b, c, d = (side.a for side in quad_sides)
    if circle_test(a, b, c, d) <= eps:
        return g

    bcd = Triangle(b, c, d)
    dab = Triangle(d, a, b)

    g = g \
        .remove_triangle(g.triangle[side]) \
        .remove_triangle(g.triangle[neigh]) \
        .add_triangle(bcd) \
        .add_triangle(dab) \

    for side in quad_sides:
        g = legalized(g, side)

    return g
