from pyrsistent import pmap, pset

from collisions import point_in_triangle
from constants import eps
from triangle import Triangle


def rotations(three_things):
    a, b, c = three_things
    return [(a, b, c), (b, c, a), (c, a, b)]


class Graph(object):
    def __init__(self,
                 triangles=pset(),
                 triangle=pmap(),
                 ccw=pmap(),
                 cw=pmap()):
        self.triangles = triangles
        self.triangle = triangle
        self.ccw = ccw
        self.cw = cw

        for side, ccw in ccw.iteritems():
            assert side.b == ccw.a
            assert side.cross(ccw.b) >= -eps

        for side, cw in cw.iteritems():
            assert side.a == cw.b
            assert side.cross(cw.a) >= -eps

    def __contains__(self, node):
        return node in self.triangle

    def add_triangle(self, t):
        g = self
        for u, ccw, cw in rotations(t.sides):
            g = Graph(g.triangles.add(t),
                      g.triangle.set(u, t),
                      g.ccw.set(u, ccw),
                      g.cw.set(u, cw))
        return g

    def remove_triangle(self, t):
        triangle_dict = self.triangle
        for side in t.sides:
            triangle_dict = triangle_dict.remove(side)

        return Graph(self.triangles.remove(t),
                     triangle_dict,
                     self.ccw,
                     self.cw)

    def triangles_containing_point(self, p):
        triangles = []
        for t in self.triangles:
            if point_in_triangle(p, t):
                triangles.append(t)
        if not 1 <= len(triangles) <= 2:
            for t in triangles:
                print(t)
            print(p)
        return triangles

    def fragmented_at(self, p):
        old_triangles = self.triangles_containing_point(p)
        lists_of_sides = [t.sides for t in old_triangles]
        all_sides = sum(lists_of_sides, [])
        hull_sides = [s for s in all_sides if s.inv not in all_sides]
        if not 3 <= len(hull_sides) <= 4:
            print(hull_sides)

        new_triangles = [Triangle(p, side.a, side.b)
                         for side in hull_sides]

        g = self
        for t in old_triangles:
            g = g.remove_triangle(t)
        for t in new_triangles:
            g = g.add_triangle(t)

        return g, hull_sides
