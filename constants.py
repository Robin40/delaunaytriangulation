eps = 1e-8

win_width, win_height = 1024, 768


class Symbol(object):
    def __init__(self, name):
        self.name = name

    def __repr__(self):
        return self.name

EDGE_OF_TRIANGLE = Symbol('EDGE_OF_TRIANGLE')
INSIDE_TRIANGLE = Symbol('INSIDE_TRIANGLE')
OUTSIDE_TRIANGLE = Symbol('OUTSIDE_TRIANGLE')
