from constants import *
from range import Range
from rectangle import Rectangle


def sgn(x):
    if abs(x) <= eps:
        return 0
    return 1 if x > 0 else -1


def point_in_circle(p, circle):
    return p.sq_distance_to(circle.center) <= circle.r*circle.r


def point_in_triangle(p, t):
    for side in t.sides:
        if side.cross(p) < -eps:
            return False
    return True


def bounding_box(points):
    xs, ys = [p.x for p in points], [p.y for p in points]
    return Rectangle(Range.bounding(xs), Range.bounding(ys))
