Hecho en pygame (python 3) usando la IDE Pycharm.
El programa depende las librerías pygame, numpy y pyrsistent.

Implementé el algoritmo incremental con intercambio de diagonales.
El intercambio de diagonales está implementado de forma eficiente.
Como se usa legalización recursiva, aumenté el límite de recursión de python en el main (el default es 1000, y daba stack overflow durante una legalización normal para casos grandes).

Estoy usando un complejo modelo de grafo (pero que funciona) donde los lados (orientados) de los triángulos son los nodos, y tienen 3 tipos de vecinos (el lado inverso visto desde el "triángulo vecino", el lado en counter-clockwise de su mismo triángulo, y el lado clockwise). Además, cada lado conoce a su propio triángulo, mientras que el grafo conoce a todos los lados y los triángulos.

No logré que el modelo de grafo más simple visto en clases funcionara, por eso hice este modelo más complejo. Contraintuitivamente, el código quedó más corto, simple, y más fácil de debuguear con este cambio de modelo.

El programa fallaba en el test de la grilla (o en general, casos con puntos colineales) antes de que nos dieran más plazo. Esto tenía 2 causas simultáneas (es decir, debía arreglar las 2)

Causa 1/2: No estaba implementado el caso en que se agrega un punto pero este está justo en el lado de un triángulo, y se generan 2 triángulos a partir de cada triángulo tocado por el punto (en general 2).

Causa 2/2: El test de punto en triángulo estaba incorrecto, ya que si el punto en cuestión era colineal a dos de los puntos del triángulo el test decía siempre que el punto estaba adentro.

Estos bugs están arreglados y ahora el test de la grilla funciona siempre (hasta ahora).

El programa usa floats para sus cálculos, así que se usa un epsilon de 1e-8 para mitigar los posibles problemas de precisión (no sé si al final esto fue útil, supongo que sí).

Los triángulos siempre se crean en sentido antihorario, lo que es testeado con assert en el constructor de la clase Triangle (en la versión actual este assert siempre se cumple, de momento).

El programa permite agregar y borrar puntos con el mouse, y la triangulación se puede hacer paso-por-paso o toda-de-una. Cuando agregar un punto falla (por ejemplo falla el assert antes mencionado), el programa se salta ese punto y se permite seguir con los siguientes.

Cuando la triangulación se hace paso por paso, un paso significa agregar un punto y realizar todas sus legalizaciones (me hubiese gustado que el programa mostrase los pasos recursivos de las legalizaciones uno por uno, pero creo que esto generaría al menos un temblor en el código, si es que no un terremoto).

Encima de cada punto aparece un número, este número indica el orden en que se van a agregar los puntos en la triangulación (los puntos se mezclan cada vez que se agrega o elimina un punto).

No está implementado el post-procesamiento donde se sacan los triángulos del borde que no son parte de la triangulación final (no era obligatorio implementarlo tampoco).

Falta documentación y el diseño de prácticamente todo es ridículamente complejo y a veces poco convencional, lo siento!! (aunque bien intencionado, me encanta complicarme la vida, no lo puedo evitar)...
Por ejemplo, la clase Graph es una estructura de datos persitente (todos los setters retornan un nuevo objeto con los cambios, sin modificar el anterior, de forma eficiente), lo cual era absolutamente innecesario.

Si hay algo que no se entiende de lo que hace mi código pregúntenme (siento que esto es muuucho más eficiente que documentarlo todo, aunque también es muy probable que me pregunten por tooodo).

Controles:
- agregar puntos con click izquierdo
- borrar un punto con click derecho sobre él
- ejecuta UN paso de la triangulación pulsando ESPACIO
- ejecuta la triangulación toda-de-una y muestra el resultado pulsando ENTER
- pone los puntos necesarios para el test de la grilla pulsando G