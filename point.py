from math import sqrt

from constants import eps


class Point(object):
    def __init__(self, x, y):
        self.x = x
        self.y = y

    @staticmethod
    def at(pos):
        return Point(pos[0], pos[1])

    def pos(self):
        return int(self.x), int(self.y)

    def __add__(self, other):
        return Point(self.x + other.x, self.y + other.y)

    def __sub__(self, other):
        return Point(self.x - other.x, self.y - other.y)

    def __truediv__(self, scale):
        return Point(self.x/scale, self.y/scale)

    def __eq__(self, other):
        return abs(self.x - other.x) <= eps and \
               abs(self.y - other.y) <= eps

    def __hash__(self):
        return hash((self.x, self.y))

    def __repr__(self):
        return "•({}, {})".format(self.x, self.y)

    def sq_norm(self):
        return self.x*self.x + self.y*self.y

    def sq_distance_to(self, other):
        return (self - other).sq_norm()

    def distance_to(self, other):
        return sqrt(self.sq_distance_to(other))

    def cross(self, other):
        return self.x*other.y - self.y*other.x
