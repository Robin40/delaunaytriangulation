import traceback
from functools import lru_cache

import pygame

import mouse
from diag_swap import big_triangles_containing, legalized
from floating_triangle import FloatingTriangle
from interactive_point import pos
from random import shuffle
from colors import *
import fonts
from point import Point
from triangle_graph import Graph


@lru_cache(maxsize=None)
def render_text_centered(surface, p, text, font, color_tuple):
    rendered = font.render(str(text), True, pygame.Color(*color_tuple))
    centered = rendered.get_rect(center=pos(p))
    return rendered, centered


def draw_text_centered(surface, p, text, font, color=black):
    rendered, centered = render_text_centered(surface, p, text, font,
                                              tuple(color))
    surface.blit(rendered, centered)


class SteppedTriangulation(object):
    def __init__(self, canvas):
        self.canvas = canvas
        self.points = []
        self.iter_points = iter([])
        self.graph = Graph()
        self.auto_step = False

    def start(self):
        points = self.canvas.model_points()
        shuffle(points)

        self.graph = Graph()
        if points:
            t1, t2 = big_triangles_containing(points)
            self.graph = Graph() \
                .add_triangle(t1) \
                .add_triangle(t2)

        self.points = points
        self.iter_points = iter(points)

    def step(self):
        p = next(self.iter_points)

        try:
            self.graph, prev_sides = self.graph.fragmented_at(p)
            for side in prev_sides:
                self.graph = legalized(self.graph, side)
        except Exception:
            traceback.print_exc()

    def update(self):
        if self.auto_step:
            try:
                self.step()
            except StopIteration:
                self.auto_step = False

        if self.canvas.changed:
            self.canvas.changed = False
            self.start()

    def draw(self, surface):
        for index, p in enumerate(self.points):
            draw_text_centered(surface, p + Point(8, -8), index, fonts.small)

        draw_text_centered(surface, mouse.p + Point(8, -8),
                           "{}, {}".format(mouse.p.x, mouse.p.y), fonts.small)

        for t in self.graph.triangles:
            FloatingTriangle(t).update().draw(surface)
            # FloatingCircle(circumcircle(triangle)).draw(surface)
