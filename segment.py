class Segment(object):
    def __init__(self, a, b):
        self.a = a
        self.b = b

    def __eq__(self, other):
        return self.a == other.a and self.b == other.b

    def __hash__(self):
        return hash((self.a, self.b))

    def __repr__(self):
        return "{}→{}".format(self.a, self.b)

    @property
    def inv(self):
        return Segment(self.b, self.a)

    def cross(self, p):
        return (self.b - self.a).cross(p - self.a)
