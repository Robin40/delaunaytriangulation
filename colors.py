from pygame import Color

black = Color('black')
blue = Color('blue')
cyan = Color('cyan')
red = Color('red')
white = Color('white')


def color_alpha(color, alpha):
    return Color(color.r, color.g, color.b, int(alpha*255))
