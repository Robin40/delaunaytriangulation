from constants import eps
from segment import Segment


class Triangle(object):
    def __init__(self, *points):
        a, b, c = points
        assert Segment(a, b).cross(c) > eps

        self.points = points
        self.sides = [Segment(points[(i + 1) % 3], points[(i + 2) % 3])
                      for i in range(3)]

    def __repr__(self):
        return "Δ({}, {}, {})".format(*self.points)

    def point(self, index):
        return self.points[index]
