import pygame
import mouse
from collisions import point_in_triangle
from interactive_point import pos
from colors import *


class FloatingTriangle(object):
    def __init__(self, triangle, color=blue, border_color=black,
                 alpha=0.2):
        self.triangle = triangle
        self.base_color = color
        self.color = color
        self.border_color = border_color
        self.alpha = alpha

    def update(self):
        # hover = point_in_triangle(mouse.p, self.triangle)
        # self.color = cyan if hover else self.base_color
        return self

    def draw(self, surface):
        points = list(map(pos, self.triangle.points))
        if False:  # self.alpha:
            color_a = color_alpha(self.color, self.alpha)
            aux = pygame.Surface(surface.get_size(), pygame.SRCALPHA)
            pygame.draw.polygon(aux, color_a, points)
            surface.blit(aux, (0, 0))
        pygame.draw.polygon(surface, self.border_color, points, 2)
        return self
