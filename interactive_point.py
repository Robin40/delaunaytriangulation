import pygame

from circle import Circle
from collisions import point_in_circle
from colors import red


def pos(p):
    return int(p.x), int(p.y)


class InteractivePoint(object):
    def __init__(self, p, color=red, r=4):
        self.p = p
        self.color = color
        self.r = r
        self.exists = True

    def draw(self, surface):
        pygame.draw.circle(surface, self.color, pos(self.p), self.r)

    def right_click(self, mouse_p):
        if point_in_circle(mouse_p, Circle(self.p, self.r)):
            self.exists = False
