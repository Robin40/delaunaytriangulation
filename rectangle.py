from point import Point


class Rectangle(object):
    def __init__(self, x_range, y_range):
        self.x_range = x_range
        self.y_range = y_range

    def with_margin(self, d):
        return Rectangle(self.x_range.with_margin(d),
                         self.y_range.with_margin(d))

    def points(self):
        return [Point(self.x_range.start, self.y_range.start),
                Point(self.x_range.stop, self.y_range.start),
                Point(self.x_range.stop, self.y_range.stop),
                Point(self.x_range.start, self.y_range.stop)]
