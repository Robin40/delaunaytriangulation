import pygame

from colors import blue
from interactive_point import pos


class FloatingCircle(object):
    def __init__(self, circle, border_color=blue):
        self.circle = circle
        self.border_color = border_color

    def draw(self, surface):
        pygame.draw.circle(surface, self.border_color, pos(self.circle.center),
                           int(self.circle.r + .5), 2)
