import sys

import pygame

import fonts
import mouse
from constants import win_width, win_height
from point import Point
from universe import Universe
assert sys.version_info[0] == 3, "Python 3 required"

__author__ = "Robinson Alexis Castro Fernández"
__email__ = "r.a.c.f.28@gmail.com"


sys.setrecursionlimit(10000)
pygame.init()
fonts.init()
surface = pygame.display.set_mode((win_width, win_height), pygame.HWSURFACE)
background_color = pygame.Color('white')

u = Universe()

done = False
while not done:
    for event in pygame.event.get():
        if event.type == pygame.QUIT:
            done = True

        if event.type == pygame.KEYDOWN:
            if event.key == pygame.K_SPACE:
                try:
                    u.canvas.triangulation.step()
                except StopIteration:
                    u.canvas.triangulation.start()

            if event.key == pygame.K_RETURN:
                u.canvas.triangulation.auto_step = True

            if event.key == pygame.K_g:
                u.canvas.arrange_grid()

        mouse_pos = pygame.mouse.get_pos()
        mouse.p = Point(mouse_pos[0], mouse_pos[1])
        mouse.react_to(event, u)

    u.update()
    u.draw(surface)
    # pygame.time.wait(1000//fps)

pygame.quit()