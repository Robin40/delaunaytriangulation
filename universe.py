import pygame

from colors import white
from interactive_canvas import InteractiveCanvas


class Universe(object):
    def __init__(self):
        self.color = white
        self.canvas = InteractiveCanvas()

    def left_click(self, p):
        self.canvas.left_click(p)

    def right_click(self, p):
        self.canvas.right_click(p)

    def draw(self, surface):
        surface.fill(self.color)
        self.canvas.draw(surface)
        pygame.display.flip()

    def update(self):
        self.canvas.update()
