import pygame
from interactive_point import InteractivePoint
from point import Point

LEFT_CLICK = 1
MIDDLE_CLICK = 2
RIGHT_CLICK = 3
SCROLL_UP = 4
SCROLL_DOWN = 5

p = None


def react_to(event, u):
    if event.type == pygame.MOUSEBUTTONDOWN:
        if event.button == LEFT_CLICK:
            u.left_click(p)

        if event.button == RIGHT_CLICK:
            u.right_click(p)
