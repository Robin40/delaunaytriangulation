from interactive_point import InteractivePoint
from constants import win_width, win_height
from point import Point
from stepped_triangulation import SteppedTriangulation


def existent(iterable):
    t = type(iterable)
    return t(filter(lambda instance: instance.exists, iterable))


class InteractiveCanvas(object):
    def __init__(self):
        self.points = set()
        self.triangulation = SteppedTriangulation(self)
        self.changed = False

    def left_click(self, p):
        self.points.add(InteractivePoint(p))
        self.changed = True

    def right_click(self, p):
        for point in self.points:
            point.right_click(p)

        new_points = existent(self.points)
        if len(new_points) != self.points:
            self.changed = True
        self.points = new_points

    def arrange_grid(self):
        self.points = set()
        for x in range(64, win_width - 63, 64):
            for y in range(64, win_height - 63, 64):
                self.left_click(Point(x, y))

    def draw(self, surface):
        for point in self.points:
            point.draw(surface)

        self.triangulation.draw(surface)

    def update(self):
        self.triangulation.update()

    def model_points(self):
        return [point.p for point in self.points]
