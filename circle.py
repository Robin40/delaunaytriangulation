from math import sqrt

import numpy as np
from numpy.linalg import det

from point import Point


class Circle(object):
    def __init__(self, center, r):
        self.center = center
        self.r = r


# def circumcircle(triangle):
#     sq_xys = [(p.sq_norm(), p.x, p.y) for p in triangle.points]
#     a = det(np.array([[x, y, 1] for sq, x, y in sq_xys]))
#     bx = det(np.array([[sq, y, 1] for sq, x, y in sq_xys]))
#     by = det(np.array([[sq, x, 1] for sq, x, y in sq_xys]))
#     c = -det(np.array([[sq, x, y] for sq, x, y in sq_xys]))
#
#     two_a = a + a
#     r = sqrt(bx*bx + by*by - 4*a*c)/abs(two_a)
#     center = Point(-bx/two_a, -by/two_a)
#     return Circle(center, r)

def circumcircle(triangle):
    a, b, c = triangle.points
    b, c = b - a, c - a
    d = 2*b.cross(c)
    sq_b, sq_c = b.sq_norm(), c.sq_norm()
    center = a + Point(c.y*sq_b - b.y*sq_c, b.x*sq_c - c.x*sq_b)/d
    r = a.distance_to(center)
    return Circle(center, r)
