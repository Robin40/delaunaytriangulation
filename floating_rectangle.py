import pygame
from colors import black


class FloatingRectangle(object):
    def __init__(self, rect, border_color=black):
        self.rect = rect
        self.border_color = border_color

    def draw(self, surface):
        xr, yr = self.rect.x_range, self.rect.y_range
        orig = int(xr.start), int(yr.start)
        size = int(len(xr)), int(len(yr))
        rect = pygame.Rect(orig, size)
        pygame.draw.rect(surface, self.border_color, rect, 1)

